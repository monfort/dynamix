import os
import dx_other_utils as dx_utils


def call_jbrowse_bed_index_script(jbrowse_perl_script_path, bed_file_path, features_trackname, output_dir, extra_args=[]):
    script_args = ['--bed', bed_file_path, '--trackLabel', features_trackname,
                           '--out', output_dir, "--trackType", "CanvasFeatures",
                           '--key', features_trackname.replace("_", " ")[0:30]]\
                           + extra_args
    try:
        dx_utils.call_jbrowse_script(jbrowse_perl_script_path, script_args)
    except OSError as index_oserr:
        raise index_oserr
    except RuntimeError as index_runerr:
        raise index_runerr
