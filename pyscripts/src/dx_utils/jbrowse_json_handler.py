import sys
import re
import os
try:
    import simplejson as json
except ImportError as sjsonh_imp_err:
    try:
      import json as json
    except ImportError as jsonh_imp_err:
       sys.exit("### [Import Error] {0}. The 'simplejson' library needs to be installed.".format(jsonh_imp_err))


def get_jbrowse_tracklist_content(jbrowse_tracklist_file):
    return read_json_file(jbrowse_tracklist_file)

##############################################################
# Read a JSON with comments
# Code from:
# http://www.lifl.fr/~damien.riquet/parse-a-json-file-with-comments.html
##############################################################

# Regular expression for comments
comment_re = re.compile(
    '(^)?[^\S\n]*/(?:\*(.*?)\*/[^\S\n]*|/[^\n]*)($)?',
    re.DOTALL | re.MULTILINE
)


def parse_json_format(jbrowse_raw_config=[]):
    jbrowse_config_content = {}
    jbrowse_raw_config_content = ''.join(jbrowse_raw_config)
    try:
        jbrowse_config_content = parse_json(jbrowse_raw_config_content)
    except ValueError as val_json_err:
        raise val_json_err
    return jbrowse_config_content


def parse_json(content):
        match = comment_re.search(content)
        while match:
            content = content[:match.start()] + content[match.end():]
            match = comment_re.search(content)
        # asset_json=json.loads(content)
        # encoded=json.encoder.JSONEncoderForHTML().encode(asset_json)
        return json.loads(content)
        # return encoded


def dumps_json_snippet(conf_snippet):
    # return json.dumps(conf_snippet, sort_keys=True)
    return json.dumps(json_data, indent=indentation, sort_keys=True, cls=json.encoder.JSONEncoderForHTML)

def dump_pretty_json(json_data, indentation=4, sort=True):
    # return json.dumps(json_data, indent=indentation, sort_keys=sort, cls=json.encoder.JSONEncoderForHTML)
    return json.dumps(json_data, indent=indentation, sort_keys=sort)

def write_json_file(json_fpath, data=[]):
    try:
        with open(json_fpath, 'w') as track_conf_file:
            if data:
                track_conf_file.write(dump_pretty_json(data))
    except IOError as write_json_ioerr:
        print "### Couldn't write track configuration snippet to file: Error : {0}".format(write_json_ioerr)
        raise write_json_ioerr
