import re
import os

def filter_content_line(jbrowse_conf_lines = []):
    new_jbrowse_conf_lines = []
    for line in jbrowse_conf_lines:
        if line and not line.startswith("#"):
            no_ws_line = "".join(line.split())
            if no_ws_line:
                new_jbrowse_conf_lines.append(no_ws_line)
    return new_jbrowse_conf_lines


def get_jbrowse_conf_dictionary(jbrowse_conf_lines):
    skip_section = tuple('trackSelector')
    jbrowse_conf_lines = filter_content_line(jbrowse_conf_lines)
    configuration_dictionary = {}
    curr_section = ""
    for line in jbrowse_conf_lines:
        if line:
            option_splitted_line = ""
            matching_section = re.match("\[(?P<section_name>.*)\]", line)
            if matching_section:  # it is a section e.g. [GENERAL]
                section_name = matching_section.group("section_name")
                curr_section = section_name.strip()
                configuration_dictionary[curr_section] = {}  # initialise section key
            else:  # its an option from a section
                if not curr_section.startswith(skip_section):
                    if not curr_section:
                        # found an option line but no section first ?, it's not the expected format
                        raise SyntaxError("The file does not follow the expected JBrowse \"conf\" format".format(jbrowse_conf_lines))
                    option_splitted_line = re.split("(\+*=)", line, 1)  # split against "=" and "+=", max once
                    if len(option_splitted_line) < 3:
                        # not the expected format...
                        raise SyntaxError("The file does not follow the expected JBrowse \"conf\" format".format(jbrowse_conf_lines))
                    option_value = ""
                    try:
                        option_key = option_splitted_line[0]
                        # option_splitted_line[1] should be the matched delimiter
                        option_value = option_splitted_line[2]
                    except KeyError as kerr:
                        print kerr
                        raise kerr
                    else:
                        try:
                            configuration_dictionary[curr_section].setdefault(option_key, []).append(option_value)
                        except KeyError as no_section:
                            print kerr
                            raise kerr
    return configuration_dictionary
