define(
    [
        "dojo/_base/declare",
        "dojo/_base/fx",
        "dojo/_base/lang",
        "dojo/_base/array",
        "dojo/_base/json",
        "dojo/dom-style",
        "dojo/dom-construct",
        "dojo/dom-attr",
        "dojo/query",
        "dojo/dom",
        "dojo/mouse",
        "dojo/on",
        "dojo/cookie",
        "dojo/Deferred",
        "dijit/form/Select",
        "dijit/form/TextBox",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",
        "dojo/text!./templates/dxTrackGroupTemplate.html",
        "dojo/domReady!"
    ],
    function(
        declare,
        baseFx,
        lang,
        array,
        JSON,
        domStyle,
        domConstruct,
        domAttr,
        query,
        dom,
        mouse,
        on,
        cookie,
        Deferred,
        select,
        textBox,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        dxGroupTemplate
    ){
        return declare ([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],{
            templateString: dxGroupTemplate,

            /* (*) attribute initialised by constructor */
            gid:"",
            gid_short:"",
            wid: "",
            parentGroup: null,
            dxName: "",
            /* User interactive elements*/
            selector: null,
            refreshButton: "butt_ref",
            addButton: "butt_plus",
            removeButton: "butt_minus",

            /* Hook attributes for masterWidget */
            widgetGroupMode : "disabled",
            // toggleSignal: false,
            // toggleFeature: false,
            groupAllShown : false,
            isMaxTracksReached: false,
            numberRelevantOnInterval : null,
            numberShownOnInterval : null,
            numberRelevantShownOnInterval : null,

            groupStateInfoCircle:null,

            /* animations and images */
            mouseAnim: null,
            stateColorCircleAnim: null,

            //colors
            backgroundColor: "#A9C6EB",
            mouseOverBackgroundColor: "#E5EDF9",
            backgroundColorMaxTrack: "#FFDB99",
            mouseOverBackgroundColorMaxTrack: "#FFF7EA",
            // state info circle
            greenStateInfoIconDom: "#00B624",
            orangeStateInfoIconDom : "#E38F00",
            // redStateInfoIconDom: "#CC0000",
            redStateInfoIconDom : "#CC0000",

            constructor: function(args) {
                dojo.safeMixin(this, args);
                this.inherited(arguments);
            },

            postMixInProperties : function() {
                //after having attributes values
                //before rendering HTML
                this.inherited(arguments);

                this.gid_short = this.gid.substring(0, 30).replace("_", " ");
            },

            postCreate: function(){
                this.inherited(arguments);
                var thisWidget = this;
                var myNode = this.domNode;
                //Color
                this.colorSquare = this["colorSquare_"+this.gid];
                if (typeof this.colorSq !== 'undefined'){
                    domStyle.set(this.colorSquare, "visibility", "visible");
                    domStyle.set(this.colorSquare, "padding-right", "3px");
                    domStyle.set(this.colorSquare, "color", this.colorSq);
                }
                //Selector
                this.selector = this["selector_"+this.gid];
                this.selector.set('value', this.widgetGroupMode, false);
                //Buttons
                this.refreshButton = this["drefresh_"+this.gid];
                this.addButton = this["dadd_"+this.gid];
                this.removeButton = this["dremove_"+this.gid];
                this.toggleSButton = this["dtoggsignal_"+this.gid];
                this.toggleFButton = this["dtoggfeature_"+this.gid];

                // icons
                this.groupStateInfoCircle = this["groupStateInfoCircle_"+this.gid];
                this.groupStateInfoCircleID = this["groupStateInfoCircle_"+this.gid].id;
                this.groupStateIsMaxTrackReachedIcon = this["groupStateMaxTracksIcon_"+this.gid].id;
                // this["groupStateInfoCircle_"+this.gid].id;
                this.groupStateIsMaxTrackReachedIcon = this["groupStateMaxTracksIcon_"+this.gid].id;

                //Own behavior
                /* this.own gather event handlers / watchers. Allow easy removal of all of them,
                ** for example when widget.destroy() is called*/
                this.thisWidgetGroupModeWatcher = this.watchThisWidgetGroupMode();
                this.own(
                    this.thisWidgetGroupModeWatcher,
                    /* WARN : this.watch fire event only if the attribute is modified with the this.set("ATTR", VAL) function
                    ********** i.e. It does not work with this.ATTR = VAL */
                    /* Event occuring in widget, thrown to update parent group information */
                    on(this.selector, "change", lang.hitch(this, function(value){
                        this.set("widgetGroupMode", value);
                    })),
                    on(this.refreshButton, "click", lang.hitch(this,"_onWidgetRefreshButtonClick")),
                    on(this.addButton, "click", lang.hitch(this, "_onWidgetAddButtonClick" )),
                    on(this.removeButton, "click", lang.hitch(this, "_onWidgetRemoveButtonClick")),
                    on(this.toggleFButton, "change", lang.hitch(this, function(isChecked){
                        this._onWidgetFeaturesButtonClick(isChecked); } )),
                    on(this.toggleSButton, "change", lang.hitch(this, function(isChecked) {
                        this._onWidgetSignalButtonClick(isChecked);
                    } )),
                    /* Event occuring in widget, thrown to update widget */
                    on(myNode, mouse.enter, lang.hitch(this, function(){
                        var bgCol = (this.parentGroup.isMaxTracksReached) ? this.mouseOverBackgroundColorMaxTrack: this.mouseOverBackgroundColor;
                        this._changeBgColor(bgCol);
                    })),
                    on(myNode, mouse.leave, lang.hitch(this, function(){
                        var bgCol = (this.parentGroup.isMaxTracksReached) ? this.backgroundColorMaxTrack : this.backgroundColor;
                        this._changeBgColor(bgCol);
                    }))
                );
                this.parentGroup.dx.dxMasterWidget.addDxGroupWidget(this);

                // Color of toggle signal / feature
                var toggleSColor = (this.parentGroup.get("toggleSignal")) ? "#00008B" : "gray"
                domStyle.set(this.toggleSButton.domNode, "color", toggleSColor);

                var toggleFColor = (this.parentGroup.get("toggleFeatures")) ? "#00008B" : "gray"
                domStyle.set(this.toggleFButton.domNode, "color", toggleFColor);

                //
                // INIT GROUP

                // parentGroupMode = this.parentGroup.get("widgetGroupMode");
                if (this.widgetGroupMode === 'automatic'
                    || this.widgetGroupMode === 'manual')
                {
                    this._updateShownTrackNumber('0');
                    this._updateRelevantTrackNumber('0');
                    this._updateRelevantShownTrackNumber('0');
                    this._enableButtons();
                    this.watchParentGroup();
                }
                else if (this.widgetGroupMode === 'disabled') this._disableButtons();
            },

            // startup(){
            //     this.widgetEnabledDone = new Deferred().resolve();
            // }

            watchThisWidgetGroupMode: function(){
                return this.watch("widgetGroupMode", lang.hitch(this, function(name, oldMode, newMode){
                    this._onWidgetGroupModeChange(oldMode, newMode);
                }));
            },

            watchParentGroup: function(){
                this.parentGroupAllShownWatcher = this.watchParentGroupAllShown();
                this.parentGroupIsMaxTracksReachedWatcher = this.watchParentGroupIsMaxTracksReached();
                this.parentGroupNumberRelevantOnIntervalWatcher = this.watchParentGroupNumberRelevantOnInterval();
                this.parentGroupNumberShownOnIntervalWatcher = this.watchParentGroupNumberShownOnInterval();
                this.parentGroupNumberRelevantShownOnIntervalWatcher = this.watchParentGroupNumberRelevantShownOnInterval();
                this.own(
                    this.parentGroupAllShownWatcher,
                    this.parentGroupIsMaxTracksReachedWatcher,
                    this.parentGroupNumberRelevantOnIntervalWatcher,
                    this.parentGroupNumberShownOnIntervalWatcher,
                    this.parentGroupNumberRelevantShownOnIntervalWatcher
                )
            },

            unwatchParentGroup: function(){
                // this.parentGroupModeWatcher.remove();
                // delete this.parentGroupModeWatcher;
                this.parentGroupAllShownWatcher.remove();
                // delete this.parentGroupAllShownWatcher;
                this.parentGroupIsMaxTracksReachedWatcher.remove();
                // delete this.parentGroupIsMaxTracksReachedWatcher;
                this.parentGroupNumberRelevantOnIntervalWatcher.remove();
                // delete this.parentGroupNumberRelevantOnIntervalWatcher;
                this.parentGroupNumberShownOnIntervalWatcher.remove();
                // delete this.parentGroupNumberShownOnIntervalWatcher;
                this.parentGroupNumberRelevantShownOnIntervalWatcher.remove();
                // delete this.parentGroupNumberRelevantShownOnIntervalWatcher;
            },


            _onWidgetGroupModeChange: function(oldMode, newMode){
                if (oldMode === 'disabled' && (newMode === 'automatic' || newMode === 'manual')){
                // if (oldMode === 'disabled' && (newMode === 'automatic' || newMode === 'manual')){
                    this._updateShownTrackNumber('0');
                    this._updateRelevantTrackNumber('0');
                    this._updateRelevantShownTrackNumber('0');
                    this._enableButtons();
                    this.watchParentGroup();
                }
                // if ((oldMode === 'manual'  || oldMode === 'automatic') && newMode === 'disabled'){
                if (oldMode !== 'disabled' && newMode === 'disabled'){
                    // this.parentGroup.set("groupAllShown", true);
                    this.disableGroupWidget();
                    this.unwatchParentGroup();
                    this._disableButtons();
                    this._changeBgColor(this.backgroundColor);
                }
            },

            watchParentGroupAllShown: function(){
                return this.parentGroup.watch("groupAllShown", lang.hitch(this, function(name, oldAllShwn, newAllShwn){
                    this.set("groupAllShown", newAllShwn); // for master widget
                    this._updateGroupStateInfoCircle(newAllShwn);
                    // this._checkGroupMissingRelevantBlinking(newAllShwn);
                }));

            },

            watchParentGroupIsMaxTracksReached: function(){
                return this.parentGroup.watch("isMaxTracksReached", lang.hitch(this, function(name, oldRchd, newRchd){
                    this.set("isMaxTracksReached", newRchd); // for master widget
                    this._groupMaxTrackChanged(oldRchd, newRchd);
                }));
            },

            watchParentGroupNumberRelevantOnInterval: function(){
                return this.parentGroup.watch("numberRelevantOnInterval", lang.hitch(this, function(name, oldVal, newRelevant){
                    this.set("numberRelevantOnInterval", newRelevant) // for master widget
                    this._updateRelevantTrackNumber(newRelevant);
                }))
            },

            watchParentGroupNumberShownOnInterval: function(){
                return this.parentGroup.watch("numberShownOnInterval", lang.hitch(this, function(name, oldVal, newShown){
                    this.set("numberShownOnInterval", newShown); // for master widget
                    this._updateShownTrackNumber(newShown);
                }))
            },

            watchParentGroupNumberRelevantShownOnInterval: function(){
                return this.parentGroup.watch("numberRelevantShownOnInterval", lang.hitch(this, function(name, oldRVal, newRShown){
                    this.set("numberRelevantShownOnInterval", newRShown); // for master widget
                    this._updateRelevantShownTrackNumber(newRShown);
                }))
            },

            _onWidgetRefreshButtonClick: function(){
                if (this.parentGroup.get("groupMode") !== "disabled") return this.parentGroup.showAndHideMultipleTracks();
                else return this.parentGroup.dx.newResolvedDummyPromise();
            },

            _onWidgetAddButtonClick: function(){
                if (this.parentGroup.get("groupMode") !== "disabled") return this.parentGroup.showMultipleTracks();
                else return this.parentGroup.dx.newResolvedDummyPromise();
            },

            _onWidgetRemoveButtonClick: function(){
                if (this.parentGroup.get("groupMode") !== "disabled") {
                    return this.parentGroup.hideShownTracks(this.parentGroup.get("labelsShownOnInterval"));
                }
                else {
                    return this.parentGroup.dx.newResolvedDummyPromise();
                }
            },

            _onWidgetSignalButtonClick: function(isChecked){
                if (isChecked !== this.parentGroup.get("toggleSignal")){
                    this.parentGroup.set("toggleSignal", isChecked);
                    if (isChecked) query("#"+this.toggleSButton.id).style("color", "#00008B");
                    else query("#"+this.toggleSButton.id).style("color", "gray");
                }
            },

            _onWidgetFeaturesButtonClick: function(isChecked){
                if (isChecked !== this.parentGroup.get("toggleFeatures")){
                    this.parentGroup.set("toggleFeatures", isChecked);
                    if (isChecked) query("#"+this.toggleFButton.id).style("color", "#00008B");
                    else query("#"+this.toggleFButton.id).style("color", "gray");
                }
            },

            _updateGroupStateInfoCircle: function(newAllShwn){
                var groupStateInfoCircleColor = (newAllShwn) ? this.greenStateInfoIconDom : this.orangeStateInfoIconDom;
                domStyle.set(this.groupStateInfoCircle, "color", groupStateInfoCircleColor);
            },

            _updateGroupMaxTracksWarningIcon: function(oldMaxTrReached, newMaxTrReached){
                if (!oldMaxTrReached && newMaxTrReached) domStyle.set(this.groupStateIsMaxTrackReachedIcon, 'display', 'inline-block');
                if (oldMaxTrReached && !newMaxTrReached) domStyle.set(this.groupStateIsMaxTrackReachedIcon, 'display', 'none');
            },

            _groupMaxTrackChanged: function(oldRchd, newRchd){
                if (!oldRchd && newRchd){
                    this._changeBgColor(this.backgroundColorMaxTrack);
                }
                else if (oldRchd && !newRchd){
                    this._changeBgColor(this.backgroundColor);
                }
            },

            _enableButtons: function(){
                this.refreshButton.setDisabled(false);
                this.addButton.setDisabled(false);
                this.removeButton.setDisabled(false);
                this.toggleSButton.setDisabled(false);
                this.toggleFButton.setDisabled(false);
                this.toggleSButton.set("checked", this.parentGroup.get("toggleSignal")); //TODO here do better, remember if it was check or not!
                this.toggleFButton.set("checked", this.parentGroup.get("toggleFeatures")); //TODO here do better, remember if it was check or not!
            },
            _disableButtons: function(){
                this.refreshButton.setDisabled(true);
                this.addButton.setDisabled(true);
                this.removeButton.setDisabled(true);
                this.toggleSButton.setDisabled(true);
                this.toggleFButton.setDisabled(true);
                this.toggleSButton.set("checked", this.parentGroup.get("toggleSignal")); //TODO here do better, remember if it was check or not!
                this.toggleFButton.set("checked", this.parentGroup.get("toggleFeatures")); //TODO here do better, remember if it was check or not!
            },

            disableGroupWidget: function(){
                this.set("numberShownOnInterval", 0);
                this.set("numberRelevantOnInterval", 0);
                this.set("numberRelevantShownOnInterval", 0);
                this._updateShownTrackNumber('-');
                this._updateRelevantTrackNumber('-');
                this._updateRelevantShownTrackNumber('-');
                this._cancelAnim(this.stateColorCircleAnim);
                domStyle.set(this.groupStateInfoCircle, "opacity", "1");
                domStyle.set(this.groupStateInfoCircle, "color", this.redStateInfoIconDom);
            },

            /* Call the update function with updatedByClick == true (force update of manual groups).
            ** If click on the refresh button : reload == true
            ** If click on the add button : reload == false
            **
            ** Stop blinking animation of state icon if relevant
            */

            /* Setters. Initialise attribute.
            ** Setters are called initially by the constructor IF default value is NOT null
            ** [Not "", 0, null ...]
            ** Setters are also called by this.set('attr', value),
            ** and listened by this.watch()
            */

            _setGroupModeAttr: function(gm){
                this._set("widgetGroupMode", gm);
            },

            _setNumberShownOnIntervalAttr: function(numShown){
                this._set("numberShownOnInterval", numShown);
            },
            _setNumberRelevantOnIntervalAttr: function(numRelevant){
                this._set("numberRelevantOnInterval", numRelevant);
            },
            _setNumberRelevantShownOnIntervalAttr: function(numRelevantShown){
                this._set("numberRelevantShownOnInterval", numRelevantShown);
            },


            _updateShownTrackNumber: function(numShown){
                domAttr.set(this.sumupShownNumber, "innerHTML", numShown);
            },

            _updateRelevantTrackNumber: function(numRelevant){
                domAttr.set(this.sumupRelevantNumber, "innerHTML", numRelevant);
            },

            _updateRelevantShownTrackNumber: function(numRelevantShown){
                domAttr.set(this.sumupRelevantShownNumber, "innerHTML", numRelevantShown);
            },

            /* Change background color on mouse hover */
            _changeBgColor: function(newColor){
                if (this.mouseAnim){
                    this.mouseAnim.stop();
                }
                this.mouseAnim = baseFx.animateProperty({
                    node: this.domNode,
                    properties: {
                        backgroundColor: newColor
                    },
                    onEnd: lang.hitch(this, function() {
                        this.mouseAnim = null;
                    })
                }).play();
            },

            _checkGroupMissingRelevantBlinking: function(){
                // WAS REMOVED
                // BLINKING IS TOO MESSY WHEN MANY GROUPS
                // if (!this.groupAllShown){
                //     this._blinkGroupStateColorCircle();
                // }
                // else if (this.stateColorCircleAnim) {
                //     this._cancelAnim(this.stateColorCircleAnim);
                //     domStyle.set(this.groupStateInfoCircle, "opacity", "1");
                // }
            },

            _blinkGroupStateColorCircle: function(){
                var stateColorCircleNode = this.groupStateInfoCircle;
                if (this.stateColorCircleAnim){
                    this._cancelAnim(this.stateColorCircleAnim);
                    domStyle.set(stateColorCircleNode, "opacity", "1");
                }
                this.stateColorCircleAnim = baseFx.animateProperty({
                    node: stateColorCircleNode,
                    duration: 650,
                    properties: {
                        opacity: { start: '1', end:  '0.45' }
                    },
                    repeat: 20
                }).play();
            },

            _cancelAnim: function(anim){
                if (anim) anim.stop(true);
                anim = null;
            }
        });
    });
