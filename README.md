Dynamix
=======

Visualisation of genomic data is fundamental for gaining insights into genome function. Yet, co-visualisation of a large number of data sets remains a challenge in all popular genome browsers and the development of new visualisation methods is needed to improve the usability and user experience of genome browsers.

The Furlong Lab presents Dynamix, a JBrowse plugin that enables the parallel inspection of hundreds of genomic data sets. Dynamix takes advantage of a priori knowledge to automatically display data tracks with signal within a genomic region of interest. As the user navigates through the genome, Dynamix automatically updates data tracks and limits all manual operations otherwise needed to ad-just the data visible on screen. Dynamix also introduces a new carousel view that optimises screen utilisation by enabling users to independently scroll through groups of tracks.

Dynamix is a plugin for the JBrowse genome browser. It is developped in the Europen Molecular Biology Laboratory - EMBL -, in Heidelberg, by the Furlong Laboratory (http://furlonglab.embl.de/ -- http://www.embl.de/research/units/genome_biology/furlong/index.html) and the Genome Biology Computational Support - GBCS (http://gbcs.embl.de/).

=================================================================================================

Plugin            :   Dynamix v1.2 (01-2017, stable)

Download link     :   Contact Author, see Author section at EOF

Introduction link :   http://furlonglab.embl.de/dynamix

Programmer        :   Matthias Monfort

Keywords          :   dynamix, plugin, jbrowse, genome, browser, dynamic, browsing, dojo, ...

=================================================================================================

For an in depth presentation, please take a look at **http://furlonglab.embl.de/dynamix**


Instalation
----------

This How-To aims at explaining step by step how to get Dynamix set-up in a JBrowse instance.

[ Pre-requisites ]
----------------------
*   A functional JBrowse instance. The latest release of JBrowse is available at http://jbrowse.org/install/.
A complete set-up guide is available at http://gmod.org/wiki/JBrowse\_Configuration\_Guide
*   A batch of files constituting the dataset. To register a track in Dynamix, one needs to provide a set of genomic interval in the form of a tab-delimited file. One tab-delimited file per track. If the user is interested in dynamically showing signal, he needs to provide a tab-delimited file describing genomic interval of interest for each signal file.

             refSeq  start   end
             ______________________________
            |chr2L   1007764 1308130       |
            |chr2L   1007764 1308130       |
            |chr2L   1007764 1308130       |
            |[...]                         |
            |______________________________|

            *refSeq:        Reference sequence where the signal is located,
                            e.g. the chromosome name.
            *start & end:   Lower and upper bound of the signal location
                            over the refSeq.

[ Important steps ]
----------------------
1. Download Dynamix [ TODO LINK ]
2. Unzip Dynamix archive
3. The folder should contain:
    * This README file,
    * The Dynamix source code (js/ folder)
    * A python script to create the needed Dynamix configuration (pyscripts/ folder). The python script import the following libraries:
        * os
        * re
        * sys
        * time
        * subprocess (to call JBrowse's perl scripts)
        * simplejson (to parse JBrowse and Dynamix configurations)
4. Indicate to JBrowse the plugin path, and its configuration in the jbrowse.conf configuration file (JBrowse/jbrowse.conf). By default dynamix configuration is created by the script at JBROWSE_ROOT/data/dynamix/

    * In jbrowse.conf, indicate Dynamix path as so:
                [ plugins.Dynamix ]
                location = plugins/Dynamix

    * In jbrowse.conf, indicate Dynamix configuration file. In the [GENERAL] section, add the following include line (the file itself is automatically created later on by the Dynamix configuration script):
                include += {dataRoot}/dynamix_conf.json


Tracks registration (plugin configuration)
======================

Tracks that are handled by Dynamix need to be registered. Upon track registration, Dynamix associates each track with a single Dynamix ID and the list of all the genomic interval where the track should be displayed. Moreover, Dynamix handles group of tracks and those groups need to be defined in the Dynamix configuration. A python script is available to register tracks and create the Dynamix configuration


One registers track by executing the dynamix python script(in JBROWSE_ROOT/plugins/dynamix/pyscripts/)

To register a feature track, the user would execute:

            python2.7 dynamix add-dx-data -g GROUP_NAME --id TRACK_UNIQ_ID -f PATH/TO/TAB/DELIM/FILE --index-features --display-name="Human readable name"

Where 
- -g indicate the group name, 
- --id the __unique__ track ID, 
- -f the path to the tab delimited file describing relevant genomic intervals. 
- --display-name is an optional parameter to define a human readable name that would be use by JBrowse interface. 

The path to the tab-delimited file can be absolute as there is an indexing step which stores the index somewhere where JBrowse can access at browsing time.


To register a signal track, the user would execute:

            python2.7 dynamix add-dx-data -g GROUP_NAME --id TRACK_UNIQ_ID2 -s ../../../data/PATH/TO/SIGNAL/FILE -f PATH/TO/TAB/DELIM/FILE --display-name="Human readable name"

Where 
- -g indicate the group name, 
- --id the __unique__ track ID, 
- -s the path to the signal file 
- -f the path to the tab delimited file describing relevant genomic intervals. 

As JBrowse is natively handling bigwig file (it reads them at browsing time), it is necessary to store it where it can access it, _e.g._ in the data folder and to give the relative path to the file to the dynamix script.

*NB:* With such registering, the tab-delimited file is only used to describe the relevant genomic intervals for the signal track as opposed to the first command line where the genomic intervals become an individual feature track. However, Dynamix is able to create both tracks at the same time, and pair them in a way that they will always be displayed with the signal over it’s features. To do so, one just needs to add the –index-features parameter in the command line.

Registering a track for the first time should create all the needed configuration files at JBROWSE_ROOT/data/dynamix. See the "Configuration details" section

Configuration details
======================

After registering tracks with the available python script, one finds the main configuration JSON file in ROOT/data/dynamix/dynamix_conf.json

Registered Dynamix tracks need to belong to a Dynamix group. Group configuration is loaded from an additional configuration file (created automatically when registering the first track)

The structure is the following:

            dynamix_conf.json:
            _______________________________________________________________________________________
                {
                    "dynamix": {
                        "default_state": "active",
                        "global_max_tracks":"100",
                        "group_configurations": {
                            "GROUP_ID": {
                                "dynamix_data": "data/GROUP_ID/GROUP_ID_locations.bed",
                                "group_track_ids": [...],
                                "group_track_name": "GROUP_ID",
                                "mode": "auto",
                                "borderCol" : "green",
                                "max_tracks": "10",
                                "order": "alpha",
                                "overlap_range": "5",
                                "show_features": "true",
                                "show_signal": "false",
                                "view_anchor" : ["after", "JBROWSE_ANCHOR_TRACK_LABEL"]
                            },
                        }
                    },
                    "include": [
                        "data/GROUP_ID/trackList.json",
                    ]
                }
            _______________________________________________________________________________________

Configuration Keys:

Global entries:
* The default\_state defines default activation state of Dynamix. **active** or **inactive**
* The global\_max\_tracks is the number of tracks that Dynamix can add to the track container on a given genomic interval. It does not take into consideration the non-dynamix track that could be added as well.

Group entries:
* borderCol         : [ STRING ]            CSS color string or hexadecimal color code
* max\_tracks       : [ STRING ]            The maximum number of tracks that can be added to the track container by one group.
* order             : [ "alpha" OR "list" ] The followed order to automatically organise visible tracks in the track container
* overlap_range (%) : [ STRING ]            If set (>0), it defines the region size outside the visible region where relevant tracks should be fetched. In % of the visible region. If e.g. 10% Dynamix will fetch relevant track at -10% and +10% of the visible region length (bp).
* show\_signal      : [ STRING ]            Toggle the display of the signal tracks
* show\_features    : [ STRING ]            Toggle the display of the features tracks (peaks)
* view\_anchor      : [ [STRING, STRING] ]  Label of the track where the group should be anchored in the view. First string is "**before**" or "**after**", second string is the JBrowse label of anchor track


Contact
======================

For any remark, question, suggestion, bug report on Dynamix please contact Matthias Monfort (see Author below).
Do not contact the author for JBrowse related issues, contact GMOD/JBrowse Support instead (see http://gmod.827538.n3.nabble.com/JBrowse-Support-f815920.html)


Author
======================

	Matthias Monfort,
	Bioinformatics Technical Officer in the Furlong lab @EMBL, Heidelberg
	mail1 : monfort@embl.de
	mail2 : monfort.matthias@gmail.com

&

	Charles Girardot,
	Head of the Genome Biology Computational Support @EMBL Heidelberg,
	mail : girardot@embl.de
