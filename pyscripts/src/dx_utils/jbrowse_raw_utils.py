import os
import dx_other_utils as dx_utils

# Native handling of BigWig files by JBrowse, the script will create a config.
         # add_bw_track.pl
         #       [ --in <input_trackList.json> ]                    \
         #       [ --out <output_trackList.json> ]                  \
         #       --label <track_label>                              \
         #       --bw_url <url_to_big_wig_file>                     \
         #       [ --key <track_key> ]                              \
         #       [ --plot ]                                         \
         #       [ --bicolor_pivot <pivot_for_changing_colors> ]    \
         #       [ --pos_color <color_for_positive_side_of_pivot> ] \
         #       [ --neg_color <color_for_negative_side_of_pivot> ] \
         #       [ --min_score <min_score> ]                        \
         #       [ --max_score <max_score> ]                        \
         #       [ -h|--help ]

def call_jbrowse_bigwig_script(script_path, bigwig_path, track_label, input_trackList, output_tracklist, extra_args=['--plot']):
    '''
    A wrapper to call jbrowse script.
    Better explicit arguments
    '''
    script_args = ['--bw_url', bigwig_path,
                           '--label', track_label,
                           '--in', input_trackList ,
                           '--out', output_tracklist] \
                           + extra_args
    try:
        dx_utils.call_jbrowse_script(script_path, script_args)
    except OSError as call_jb_oserr:
        raise call_jb_oserr
    except RuntimeError as subp_runerror:
        raise subp_runerror
