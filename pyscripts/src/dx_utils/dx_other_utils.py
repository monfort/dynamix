import os
import sys
import subprocess

#for debug
import jbrowse_json_handler

sizesuffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']


def read_jbrowse_configuration_file(jbrowse_config_path):
    raw_configuration_content = []
    config_bname = os.path.basename(jbrowse_config_path)
    # print "### Reading JBrowse configuration file {0}".format(config_bname)
    try:
        with open(jbrowse_config_path, 'r') as jbrowse_config:
            raw_configuration_content = jbrowse_config.readlines()
    except IOError as read_ioerr:
        raise read_ioerr
    return raw_configuration_content


def call_bash_process(args_list):
    '''
    Call a perl script in a child process (using the subprocess library).
    This is used to call jbrowse perl scripts that are used to interact with configuration
    files (e.g. trackList.json)
    '''
    ### Symplify call argument for printing
    script_path = args_list[0]
    print "### [Execute] {0}".format(" ".join([os.path.basename(script_path)] + args_list[1:]))
    try:
        p_stdout = subprocess.check_output(args_list, stderr=subprocess.STDOUT)
    except OSError as bash_call_oserr:
        # This error is raised if the script does not exist / not executable
        print "### [ERROR] OSError Exception raised calling {0}. {1}".format(script_path, bash_call_oserr)
        raise bash_call_oserr
    except ValueError as bash_call_valerror:
        # ValueError is raised if the function is called with wrong argument.
        # But The passed arguments are controlled. This should never happen
        sys.exit("ValueError was raised by subprocess. This WILL never happen... Err: {0}".format(bash_call_valerror))
    except subprocess.CalledProcessError as subprocess_error:
        print "### [ERROR] CalledProcessError Exception raised calling {0} (full cmd: {1})".format(script_path, subprocess_error.cmd)
        print "### [ERROR] Return code : {0}.".format(subprocess_error.returncode)
        print "### [ERROR] Output : {0}.".format(subprocess_error.output)
        raise subprocess_error
    else:
        pass
        # print "### [Done] [StdOut] \"{0}\"".format(p_stdout)


def call_jbrowse_script(jbrowse_script_path, args_list=[]):
    '''
    Wrapper to call bash process. Change the type of the CalledProcessError Exception if this occurs.
    Raise the exception as a RuntimeError instead, to avoid importing the subprocess lib in all the modules
    '''
    try:
        call_bash_process([jbrowse_script_path] + args_list)
    except OSError as call_jb_oserr:
        raise call_jb_oserr
    except subprocess.CalledProcessError as subprocess_error:
        print "[DEBUG] Raising a RuntimeError instead of ", type(subprocess_error)
        raise RuntimeError("### [ERROR] Failed running {0}. Exist status is ".format(subprocess_error.cmd, subprocess_error.returncode))

##
## Human readble file size
## from: http://stackoverflow.com/questions/14996453/python-libraries-to-calculate-human-readable-filesize-from-bytes
##

def humansize(nbytes):
    if nbytes == 0:
        return '0 B'
    i = 0
    while nbytes >= 1024 and i < len(sizesuffixes)-1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, sizesuffixes[i])


def get_file_size(file_path):
    file_size = "NaN"
    try:
        file_size = humansize(os.path.getsize(file_path))
    except IOError as ioerr_exc:
        raise ioerr_exc
    return file_size



def validate_usr_yes_answer():
    print "(y/n/all) ",
    try:
        raw_usr_in = raw_input()
    except KeyboardInterrupt as usr_quit_exc:
        sys.exit("\n### Bye bye...(calling sys.exit())")
    else:
        if raw_usr_in.strip().lower() in ['all']:
            print "### Answered \"all\""
            return 'all'
        else:
            if raw_usr_in.strip().lower() in ['yes', 'y']:
                print "### Answered \"{0}\" for yes.".format(raw_usr_in)
                return True
            else:
                print "### Answered \"{0}\" for no.".format(raw_usr_in)
                return False
